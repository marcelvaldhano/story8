from django.shortcuts import render
from django.http import JsonResponse
import json
import requests
# Create your views here.
def index(request):
    return render(request,'listbuku.html')

def searchBook(request,title):
    response=requests.get('https://www.googleapis.com/books/v1/volumes?q=' + title)
    return JsonResponse(response.json())