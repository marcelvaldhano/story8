$(document).ready(function(){
    searchBook("programming");
});

function readInput(){
	var title = $('#input').val();
	$('#input').val("");
	searchBook(title);
}

function searchBook(title){
    $("tbody").empty();
	$.ajax({
		url: "searchBook/" + title,
		success: function(list){
			if(list.totalItems != 0){
				list.items.forEach(function(book, index){
                    $("tbody").append(`
						<tr>
							<td>${index + 1}</td>
							<td><img src="${book.volumeInfo.imageLinks.smallThumbnail}"></td>
							<td>${book.volumeInfo.title}</td>
							<td>${book.volumeInfo.authors}</td>
							<td>${book.volumeInfo.publisher}</td>
							<td>${book.volumeInfo.pageCount}</td>
						</tr>
					`);
				});
			} else{
				$("tbody").append(`
					<tr>
						<td colspan=6 class="text-center">No search results found </td>
					</tr>
				`);
			}
		}
	});
}
