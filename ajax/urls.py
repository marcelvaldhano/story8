from django.urls import path
from . import views

urlpatterns=[
    path('',views.index,name='index'),
    path('searchBook/<title>', views.searchBook, name='searchBook'),
]